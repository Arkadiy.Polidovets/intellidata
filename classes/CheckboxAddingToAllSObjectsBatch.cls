/**
 * Created by Arkadiy Polidovets on 2/4/2019.
 */

public without sharing class CheckboxAddingToAllSObjectsBatch implements Database.Batchable<Integer>, Database.Stateful, Database.AllowsCallouts {

    public List<MetadataService.CustomField> customFields;
    public MetadataService.MetadataPort service;
    public MetadataService.Profile profile;
    public Integer counter = 0;
    public Map<String, MetadataService.ProfileLayoutAssignment> mapObjectToLayoutAssignments;

    public CheckboxAddingToAllSObjectsBatch(List<MetadataService.CustomField> customFields, MetadataService.MetadataPort service, MetadataService.Profile profile, Map<String, MetadataService.ProfileLayoutAssignment> mapObjectToLayoutAssignments) {
        this.customFields = customFields;
        this.service = service;
        this.profile = profile;
        this.mapObjectToLayoutAssignments = mapObjectToLayoutAssignments;
    }

    public Iterable<Integer> start(Database.BatchableContext context) {
        List<Integer> values = new List<Integer>();
        while(values.size() < customFields.size()) {
            values.add(values.size());
        }
        return values;
    }

    public void execute(Database.BatchableContext context, List<Integer> values) {
        for (Integer i = 0; i < values.size(); i++) {
            counter = values[i];

            MetadataService.CustomField customField = customFields[counter];
            String customFieldFullName = customField.fullName;

            String objectName = customFieldFullName.substringBefore('.');
            String fieldName = customFieldFullName.substringAfter('.');

            service.createMetadata(new MetadataService.Metadata[] { customField });

            profile = CheckboxAddingToLayout.createLayoutCopyAndAddField(service, profile, objectName, fieldName, mapObjectToLayoutAssignments);
        }
    }

    public void finish(Database.BatchableContext context) {
        System.debug('$$$ finish counter ' + counter);
        for (MetadataService.ProfileLayoutAssignment profileLayoutAssignment  : profile.layoutAssignments) {
            System.debug('$$$ finish profileLayoutAssignment.layout ' + profileLayoutAssignment.layout);
        }

        MetadataService.SaveResult[] profileSaveResults = service.updateMetadata(new MetadataService.Metadata[]{ profile });
        for(MetadataService.SaveResult profileSaveResult : profileSaveResults) {
            System.debug('$$$ finish profileSaveResult --- ' + profileSaveResult);
        }
    }

}