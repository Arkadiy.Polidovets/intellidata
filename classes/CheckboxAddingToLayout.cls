/**
 * Created by Arkadiy Polidovets on 2/6/2019.
 */

public class CheckboxAddingToLayout {
    public static MetadataService.Profile createLayoutCopyAndAddField(MetadataService.MetadataPort service, MetadataService.Profile profile, String objectName, String fieldName, Map<String, MetadataService.ProfileLayoutAssignment> mapObjectToLayoutAssignments) {
        MetadataService.ProfileLayoutAssignment profileLayoutAssignment = mapObjectToLayoutAssignments.get(objectName);

        if (profileLayoutAssignment != null) {
            String layoutName = profileLayoutAssignment.layout;
            MetadataService.Layout newLayoutWithField = getNewLayoutWithField(service, layoutName, fieldName);
            service.createMetadata(new MetadataService.Metadata[]{ newLayoutWithField });

            String nameOfLayout = newLayoutWithField.fullName;
            Integer indexOfElement = profile.layoutAssignments.indexOf(profileLayoutAssignment);
            profile.layoutAssignments[indexOfElement].layout = nameOfLayout;

            MetadataService.ProfileFieldLevelSecurity profileFieldLevelSecurity = new MetadataService.ProfileFieldLevelSecurity();
            profileFieldLevelSecurity.editable = true;
            profileFieldLevelSecurity.readable = true;
            profileFieldLevelSecurity.field = objectName + '.' + fieldName;
            profile.fieldPermissions.add(profileFieldLevelSecurity);
            System.debug('$$$ profile.layoutAssignments[indexOfElement].layout ' + profile.layoutAssignments[indexOfElement].layout);
        }
        return profile;
    }

    public static MetadataService.Layout getNewLayoutWithField(MetadataService.MetadataPort service, String layoutName, String fieldName) {
        MetadataService.Layout layout = (MetadataService.Layout) service.readMetadata('Layout', new String[] { layoutName }).getRecords()[0];
        layout.fullName = layout.fullName + '_IntelliDataCheckbox_1';

        if(layout.layoutSections==null) {
            layout.layoutSections = new List<MetadataService.LayoutSection>();
        }

        MetadataService.LayoutSection newLayoutSection = new MetadataService.LayoutSection();
        newLayoutSection.detailHeading = true;
        newLayoutSection.editHeading = true;
        newLayoutSection.style = 'OneColumn';
        MetadataService.LayoutColumn newLayoutColumn = new MetadataService.LayoutColumn();
        MetadataService.LayoutItem newLayoutItem = new MetadataService.LayoutItem();
        newLayoutItem.field = fieldName;
        newLayoutItem.behavior = 'Edit';
        newLayoutColumn.layoutItems = new List<MetadataService.LayoutItem> { newLayoutItem };
        newLayoutSection.layoutColumns = new List<MetadataService.LayoutColumn> { newLayoutColumn };
        layout.layoutSections.add(newLayoutSection);

        return layout;
    }
}