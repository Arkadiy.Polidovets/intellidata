/**
 * Created by Arkadiy Polidovets on 2/4/2019.
 */

public without sharing class CheckboxAddingToAllSObjects {

    public static MetadataService.MetadataPort service = createService();

    public static void checkboxCreating(String fieldName, Integer batchScope) {
        Map<String, Schema.SObjectType> mapOfObjects = Schema.getGlobalDescribe();
//        Set<String> objectNamesSet = mapOfObjects.keySet();
//        List<String> objectNames = new List<String>(objectNamesSet);
        List<String> objectNames = new List<String> {'contact'};

        List<MetadataService.CustomField> customFields = new List<MetadataService.CustomField>();
        for (Integer i = 0; i < objectNames.size(); i++) {
            MetadataService.CustomField customField = new MetadataService.CustomField();
            customField.fullName = objectNames[i].capitalize() + '.' + fieldName + '__c';
            customField.label = fieldName;
            customField.type_x = 'Checkbox';
            customField.defaultValue = 'false';
            customFields.add(customField);
        }

        MetadataService.Profile profile = getProfile('Admin');
        Map<String, MetadataService.ProfileLayoutAssignment> mapObjectToLayoutAssignments = getProfileLayoutAssignments(profile);
        CheckboxAddingToAllSObjectsBatch batch = new CheckboxAddingToAllSObjectsBatch(customFields, service, profile, mapObjectToLayoutAssignments);
        Database.executeBatch(batch, batchScope);
    }

    public static MetadataService.MetadataPort createService() {
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = UserInfo.getSessionId();
        return service;
    }

    public static MetadataService.Profile getProfile(String profileName) {
        MetadataService.Profile profile = (MetadataService.Profile)service.readMetadata('Profile', new String[] { profileName }).getRecords()[0];
        profile.tabVisibilities = new MetadataService.ProfileTabVisibility[] {};
        profile.applicationVisibilities = new MetadataService.ProfileApplicationVisibility[] {};
        profile.categoryGroupVisibilities = new MetadataService.ProfileCategoryGroupVisibility[] {};
        profile.classAccesses = new MetadataService.ProfileApexClassAccess[] {};
        profile.customPermissions = new MetadataService.ProfileCustomPermissions[] {};
        profile.externalDataSourceAccesses = new MetadataService.ProfileExternalDataSourceAccess[] {} ;
        profile.fieldPermissions = new  MetadataService.ProfileFieldLevelSecurity[] {};
        profile.loginIpRanges = new MetadataService.ProfileLoginIpRange[] {};
        profile.objectPermissions = new MetadataService.ProfileObjectPermissions[] {};
        profile.pageAccesses = new MetadataService.ProfileApexPageAccess[] {};
        profile.profileActionOverrides = new MetadataService.ProfileActionOverride[] {};
        profile.recordTypeVisibilities = new MetadataService.ProfileRecordTypeVisibility[] {};
        profile.tabVisibilities = new MetadataService.ProfileTabVisibility[] {};
        profile.userPermissions = new MetadataService.ProfileUserPermission[] {};
        return profile;
    }

    public static Map<String, MetadataService.ProfileLayoutAssignment> getProfileLayoutAssignments(MetadataService.Profile profile) { //profileName = 'Admin'
        Map<String, MetadataService.ProfileLayoutAssignment> mapObjectToLayoutAssignments = new Map<String, MetadataService.ProfileLayoutAssignment>();
        for(MetadataService.ProfileLayoutAssignment layoutAssignment : profile.layoutAssignments) {
            String layout = layoutAssignment.layout;
            String layoutObject = layout.split('-')[0];
            mapObjectToLayoutAssignments.put(layoutObject, layoutAssignment);
        }
        return mapObjectToLayoutAssignments;
    }
}