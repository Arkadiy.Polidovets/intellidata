/**
 * Created by Arkadiy Polidovets on 2/5/2019.
 */

global without sharing class CheckboxAddingToAllSObjectsPostInstall implements InstallHandler {
    global void onInstall(System.InstallContext context) {
        if (context.previousVersion() == null) {
            CheckboxAddingToAllSObjects.checkboxCreating('Admin', 20);
        }
    }
}